ics-ans-beast
=============

Ansible playbook to install BEAST on CentOS 7.

Variables
---------

Default variables are set in `group_vars/beast`::

    beast_rdb_user: alarm
    beast_rdb_password: alarmpwd

    beast_jms_user: alarm
    beast_jms_password: alarmpwd

    activemq_users:
      - name: admin
        password: adminpwd
      - name: "{{ beast_jms_user }}"
        password: "{{ beast_jms_password }}"

This is the minimum to pass to change the passwords used.

Check the `ics-ans-role-activemq <https://bitbucket.org/europeanspallationsource/ics-ans-role-activemq>`_,
and `ics-ans-role-beast <https://bitbucket.org/europeanspallationsource/ics-ans-role-beast>`_ roles
for the complete list of variables you can change.


Usage
-----

The playbook can be run locally or from an Ansible server.

To run locally on the machine
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

A script is provided to easily provision a machine.
It will install ansible locally.
This is for testing purpose and not recommended for production.

Download the script `bootstrap-local.sh` and run it as root::

    $ cd /tmp
    $ curl -O https://bitbucket.org/europeanspallationsource/ics-ans-beast/raw/master/bootstrap-local.sh
    $ chmod a+x /tmp/bootstrap-local.sh
    $ sudo /tmp/bootstrap-local.sh

This will use the default variables. If you want to make changes, you can use the "--no-run" option
and run manually the ansible-playbook command::

    $ sudo /tmp/bootstrap-local.sh --no-run
    Set variables in /etc/ansible/host_vars/localhost
    $ sudo /usr/local/bin/ansible-playbook /etc/ansible/playbook.yml


To run from an Ansible server
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

You must have ansible >= 2.3.0.0 already installed.
You should be able to ssh to the server to install and have sudo rights.

::

    $ git clone https://bitbucket.org/europeanspallationsource/ics-ans-beast.git
    $ cd ics-ans-beast
    # Edit the "hosts" file
    # Change the database password in group_vars/beast or create a host_vars/<hostname> file
    # with the variables to change
    $ make playbook


Alarm Configuration
-------------------

The alarm configuration is kept in a git repository.
See https://gitlab.esss.lu.se/ics-infrastructure/beast-config for more information.

Testing
-------

The playbook is tested using `molecule <https://molecule.readthedocs.io/en/master/>`_ (1.24 <= version < 2.0)::

    $ molecule test


License
-------

BSD 2-clause
