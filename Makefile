.PHONY: help roles playbook

help:
	@echo "Please use \`make <target>' where <target> is one of"
	@echo "  roles       to install the roles using ansible-galaxy"
	@echo "  playbook    to run the playbook"

roles:
	ansible-galaxy install -r roles/requirements.yml --force -p roles/

playbook: roles
	ansible-playbook -i hosts -k -K playbook.yml
