import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    '.molecule/ansible_inventory').get_hosts('beast')


def test_activemq_running(host):
    assert host.service('activemq').is_running


def test_mariadb_running(host):
    assert host.service('mariadb').is_running


def test_alarm_server_running(host):
    assert host.service('alarm-server@MAIN').is_running
